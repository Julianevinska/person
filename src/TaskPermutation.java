import java.util.HashSet;
import java.util.Set;

public class TaskPermutation {

    TaskPermutation(){}

    Set<String> getAllPermutations(String stringForPermutation) {
        Set<String> allPermutations = new HashSet<>();
        if (stringForPermutation == null) {
            return null;
        } else if (stringForPermutation.length() == 0) {
            allPermutations.add("");
            return allPermutations;
        }
        char keySymbol = stringForPermutation.charAt(0);
        String substringForPermutations = stringForPermutation.substring(1);
        Set<String> allPermutationsSubstring = getAllPermutations(substringForPermutations);
        for (String permutations : allPermutationsSubstring) {
            for (int i = 0; i <= permutations.length(); i++) {
                allPermutations.add(insertChar(permutations, keySymbol, i));
            }
        }
        return allPermutations;
    }

    private String insertChar(String permutationString, char keySymbol, int lastSymbol) {
        String firstPartOfString = permutationString.substring(0, lastSymbol);
        String secondPartOfString = permutationString.substring(lastSymbol);
        return firstPartOfString + keySymbol + secondPartOfString;
    }
}