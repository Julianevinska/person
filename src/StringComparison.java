public class StringComparison {
    private String firstString;
    private String secondString;

    StringComparison(String firstString, String secondString) {
        this.firstString = firstString;
        this.secondString = secondString;
        if(firstString.equals(" ") || secondString.equals(" ")
                || (firstString.equals("")) || (secondString.equals(""))) {
            System.out.println("String has invalid value");
        }
    }

    boolean ifIdentity() {
        char[] firstStringToChar = firstString.toLowerCase().toCharArray();
        char[] secondStringToChar = secondString.toLowerCase().toCharArray();
        int index = 0;
        if((firstString.equals("")) || (secondString.equals(""))
                || firstString.equals(" ") || secondString.equals(" ")) {
            index = -1;
        }
        if (firstStringToChar.length == secondStringToChar.length) {
            for (int i = 0; i < secondStringToChar.length; i++) {
                if (firstString.toLowerCase().contains(String.valueOf(secondStringToChar[i]))) {
                    index++;
                }
            }
        }
        return index == firstStringToChar.length;
    }
}