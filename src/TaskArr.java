import java.util.Arrays;

public class TaskArr {
    private int[] arr;

    TaskArr(){
    }

    void setArr(int[] arr) {
        this.arr = arr;
    }

    String arraySort(){
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int)Math.pow(arr[i],2);
        }
        Arrays.sort(arr);
        return Arrays.toString(arr);
    }
}