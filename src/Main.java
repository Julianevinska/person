public class Main {

    public static void main(String[] args) {

        Person task1 = new Person("Иванов Иван Иванович");
        task1.show();

        System.out.println();
        TaskArr task2 = new TaskArr();
        task2.setArr(new int[]{2, 6, 9, -6, 24, -78, 5});
        System.out.println(task2.arraySort());

        TaskPermutation task3 = new TaskPermutation();
        System.out.println(task3.getAllPermutations("julia"));

        StringComparison task4 = new StringComparison("julia", "julia");
        System.out.println(task4.ifIdentity());
    }
}