public class Person {
    private String lastName;
    private String name;
    private String patronymic;
    private String fullName;

    public Person(String lastName, String name, String patronymic) {
        this.lastName = lastName;
        this.name = name;
        this.patronymic = patronymic;
    }

    public Person(String fullName) {
        this.fullName = fullName;
        String[] fullNameArr = fullName.split(" ");
        this.lastName = fullNameArr[0];
        this.name = fullNameArr[1];
        this.patronymic = fullNameArr[2];
    }

    public void show(){
        System.out.printf("Person\nLast name: %s, Name: %s, Patronymic: %s.", this.lastName, this.name,
                this.patronymic);
    }
}